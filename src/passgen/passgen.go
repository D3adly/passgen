package main

import (
	"fmt"
	"flag"
	"strings"
	"math/rand"
	"time"
	"bytes"
)

var password bytes.Buffer
var fullString []string


func createCharSets() map[string] string {
	charSets := make(map[string]string)

	charSets["l"] = "abcdefghjkmnpqrstuvwxyz"
	charSets["u"] = "ABCDEFGHJKMNPQRSTUVWXYZ"
	charSets["d"] = "0123456789"
	charSets["s"] = "!@#$%&*?"

	return charSets
}

func main() {
	length := flag.Int("l", 8, "password length")
	clean := flag.Bool("c", false, "Print password only (no headers)")
	flag.Parse()

	charSets := createCharSets()
	rand.Seed(time.Now().Unix())
	for k := range charSets {
		strSlice := strings.Split(charSets[k],"")
		password.WriteString(strSlice[rand.Intn(len(strSlice))])
		fullString = append(fullString, strSlice...)
	}

	for i := 0; i < *length; i ++ {
		password.WriteString(fullString[rand.Intn(len(fullString))])
	}

	if *clean == false {
		fmt.Println("Secure password generator")
		fmt.Printf("Selected length: %d", *length )
		fmt.Println()
		fmt.Print("Password: ")
	}
	generatedPassword := strings.Split(password.String(),"")

	for i:= range generatedPassword {
		j:= rand.Intn(i+1)
		generatedPassword[i], generatedPassword[j] = generatedPassword[j], generatedPassword[i]
	}

	fmt.Println(strings.Join(generatedPassword,""))

}
